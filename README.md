# Employee Page

This is a pure scss design for tretton37 employee page

## Installation and Development

After cloning the project run these commands to run the application.

```bash
npm i
npm start
```

## Build

To build for production, use this command below.

```bash
npm build
```

## Features

No UI frameworks were added in this project.\
No animation librarys were added in this project and the animation is pure scss as well.\
This project is fully responsive. it uses media queries and css flex to achieve this goal.\
Above the Employee section there is a filter box.\
Employees can be filtered based on their: Name, Office, Twitter, LinkedIn, Github.\
Right beside the filter box is a button for switching between grid and list views.\
At the bottom of the employee page you can see a load more button.\
Project runs in chrome, firefox, edge perfectly fine.\
Deployed the project on heroku servers.

## Details

The reason I decided to go with pure scss instead of UI librarys was to challenge myself and have more creative power.\
Pure scss animations were perfectly fine for a project like this, instead of using heavy librarys and overengineering.\
For the filtering functionality and swichting between full and filtered data throughout the application I decided to use ContextApi.\
A global state manger is Always a good Idea when you are dealing with a large data, and specially if you wanna dispatch some changes into that data, I could have used Redux, but I don't think using Redux could have bring anything more to the table for this specific light project.\
And I created a .env file to keep the token and endpoint which were sent to me for calling the employee api.

## Demo

You can see the Demo of this project on heroku server with this [Link](https://tretton37employee.herokuapp.com)
