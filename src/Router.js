import { BrowserRouter, Switch, Route } from "react-router-dom";
import home from "./pages/Home/home";
import notFound from "./pages/NotFound/notFound";

function Router() {
  return (
    <BrowserRouter>
      <Switch>
        <Route exact path="/" component={home} />
        <Route component={notFound} />
      </Switch>
    </BrowserRouter>
  );
}

export default Router;
