const initialState = {
  initial_data: [],
  filtered_data: [],
};

function reducer(state, action) {
  switch (action.type) {
    case "SET_INITIAL_DATA":
      return { ...state, initial_data: action.payload };
    case "SET_FILTERED_DATA":
      let temp_data = state.initial_data;
      temp_data = temp_data.filter((item) => {
        if (item[`${action.payload.field}`]) {
          return !item[`${action.payload.field}`]
            .toLowerCase()
            .indexOf(action.payload.value.toLowerCase());
        }
        return false;
      });

      if (!temp_data.length) {
        alert("NO RESULTS FOUND");
        return { ...state };
      } else {
        return { ...state, filtered_data: temp_data };
      }
  }
}

export default { reducer, initialState };
