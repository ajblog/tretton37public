import React, { useReducer, useContext } from "react";
import employeeStore from "./employee";

const Context = React.createContext();

const ContextProvider = ({ children }) => {
  const contextValue = useReducer(
    employeeStore.reducer,
    employeeStore.initialState
  );
  return <Context.Provider value={contextValue}>{children}</Context.Provider>;
};

const useEmployeeStore = () => {
  return useContext(Context);
};

export { ContextProvider, useEmployeeStore };
