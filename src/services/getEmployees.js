import axios from "axios";

function getEmployees() {
  return axios.get(process.env.REACT_APP_API_ADDRESS, {
    headers: {
      Authorization: process.env.REACT_APP_TOKEN,
    },
  });
}

export default getEmployees;
