import React from "react";
import "./style.scss";
import { IoLogoLinkedin, IoMail, IoCall } from "react-icons/io5";

export default function EmployeeCard({ employee, setCurrentEmployee }) {
  return (
    <div
      onClick={() => {
        setCurrentEmployee(employee);
      }}
      onMouseEnter={() => {
        document.body.style.backgroundColor = "black";
        document.querySelector(".submit-search-btn").style.backgroundColor =
          "#016111";
        document.querySelector(".search-input-value").style.backgroundColor =
          "#016111";
        document.querySelector(".search-input-value").style.border =
          "1px solid black";
      }}
      onMouseLeave={() => {
        document.body.style.backgroundColor = "#016111";
        document.querySelector(".submit-search-btn").style.backgroundColor =
          "black";
        document.querySelector(".search-input-value").style.backgroundColor =
          "black";
        document.querySelector(".search-input-value").style.border =
          "1px solid #016111";
      }}
      className="employee-card"
    >
      <div className="employee-card-inner">
        <div className="employee-card-front">
          <img src={employee.imagePortraitUrl} alt="Avatar" />
        </div>
        <div className="employee-card-back">
          <h1>{employee.name}</h1>
          <div className="employee-socail-acounts">
            <a
              target="blank"
              href={`https://www.linkedin.com${employee.linkedIn}`}
            >
              <IoLogoLinkedin size={22} color="#f3f3f3" />
            </a>
            <a href={`mailto:${employee.email}`}>
              <IoMail size={22} color="#f3f3f3" />
            </a>
            <a href={`tel:${employee.phoneNumber}`}>
              <IoCall size={21} color="#f3f3f3" />
            </a>
          </div>
        </div>
      </div>
    </div>
  );
}
