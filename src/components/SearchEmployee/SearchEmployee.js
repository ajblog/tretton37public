import React, { useEffect, useState } from "react";
import { useEmployeeStore } from "../../store";
import {
  IoFilter,
  IoList,
  IoGrid,
  IoAdd,
  IoGridOutline,
} from "react-icons/io5";

import "./style.scss";

export default function SearchEmployee() {
  const [filterField, setFilterField] = useState("");
  const [filterValue, setFilterValue] = useState("");
  const [viewMode, setViewMode] = useState("grid");
  const [myError, setMyError] = useState(false);
  const [employeeStore, dispatch] = useEmployeeStore();

  return (
    <div className="search-container">
      {viewMode === "grid" && window.screen.width > 1024 ? (
        <IoList
          onClick={() => {
            setViewMode("list");
            let cards = document.querySelectorAll(".card");
            let imgs = document.querySelectorAll(".employee-card");
            Array.from(cards).forEach((card) => (card.style.width = "100%"));
            Array.from(imgs).forEach((img) => {
              img.style.width = "100%";
              img.style.height = "750px";
            });
          }}
          size={21}
          color="#f3f3f3"
        />
      ) : viewMode === "list" && window.screen.width > 1024 ? (
        <IoGridOutline
          onClick={() => {
            setViewMode("grid");
            let cards = document.querySelectorAll(".card");
            let imgs = document.querySelectorAll(".employee-card");
            Array.from(cards).forEach((card) => (card.style.width = "auto"));
            Array.from(imgs).forEach((img) => {
              img.style.width = "220px";
              img.style.height = "300px";
            });
          }}
          size={21}
          color="#f3f3f3"
        />
      ) : null}

      <div className="dropdown">
        <div className="dropdown-first">
          <IoFilter size={21} color="#f3f3f3" />
          {filterField.length ? filterField.toUpperCase() : "SELECT A FIELD"}
        </div>
        <div className="dropdown-content">
          <p onClick={(e) => setFilterField("name")}>FILTER BY NAME</p>
          <p onClick={(e) => setFilterField("office")}>FILTER BY OFFICE</p>
          <p onClick={(e) => setFilterField("twitter")}>FILTER BY TWITTER</p>
          <p onClick={(e) => setFilterField("linkedIn")}>FILTER BY LINKEDIN</p>
          <p onClick={(e) => setFilterField("gitHub")}>FILTER BY GITHUB</p>
        </div>
      </div>
      <div className="search-input-section">
        <input
          placeholder="WHAT YOU SEARCHING FOR?"
          className="search-input-value"
          type="text"
          value={filterValue}
          onChange={(e) => setFilterValue(e.target.value)}
        ></input>
      </div>
      <button
        onClick={() => {
          if (filterField.length) {
            setMyError(false);
            dispatch({
              type: "SET_FILTERED_DATA",
              payload: {
                field: filterField,
                value: filterValue,
              },
            });
          } else {
            setMyError(true);
          }
        }}
        className="submit-search-btn"
      >
        SUBMIT
      </button>
      {myError && <p className="error">*MAKE SURE TO FILL EVERYTHING</p>}
    </div>
  );
}
