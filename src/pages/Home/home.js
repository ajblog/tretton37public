import React, { useState, useEffect } from "react";
import EmployeeCard from "../../components/EmployeeCard/EmployeeCard";
import Loading from "../../components/Loading/Loading";
import SearchEmployee from "../../components/SearchEmployee/SearchEmployee";
import getEmployees from "../../services/getEmployees";
import { useEmployeeStore } from "../../store";

import "./style.scss";

export default function Home() {
  let filters = ["byName", "byOffice", "byTwitter", "byLinkedIn"];
  const [currentEmployee, setCurrentEmployee] = useState({});
  const [loadCount, setLoadCount] = useState(1);
  const [employeeStore, dispatch] = useEmployeeStore();
  useEffect(async () => {
    let await_data = await getEmployees();
    dispatch({
      type: "SET_INITIAL_DATA",
      payload: await_data.data,
    });
  }, []);

  useEffect(() => {
    const change_div = document.querySelector(".current-employee-maintext");
    if (change_div) change_div.innerHTML = currentEmployee.mainText;
  }, [currentEmployee]);
  if (employeeStore.initial_data.length)
    return (
      <div className="homeContainer">
        <div className="first-col">
          <SearchEmployee filters={filters} />
          {!employeeStore.filtered_data.length
            ? employeeStore.initial_data
                .slice(0, loadCount * 16)
                .map((employee, index) => {
                  return (
                    <div className="card">
                      <EmployeeCard
                        employee={employee}
                        key={index}
                        setCurrentEmployee={setCurrentEmployee}
                      />
                    </div>
                  );
                })
            : employeeStore.filtered_data
                .slice(0, loadCount * 16)
                .map((employee, index) => {
                  return (
                    <div className="card">
                      <EmployeeCard
                        employee={employee}
                        key={index}
                        setCurrentEmployee={setCurrentEmployee}
                      />
                    </div>
                  );
                })}
          {employeeStore.initial_data.length !==
            employeeStore.initial_data.slice(0, loadCount * 16).length &&
          employeeStore.filtered_data.length === 0 ? (
            <button
              onClick={() => setLoadCount(loadCount + 1)}
              className="load-more-btn"
            >
              ...LOAD MORE...
            </button>
          ) : employeeStore.filtered_data.length > 0 &&
            employeeStore.filtered_data.length !==
              employeeStore.filtered_data.slice(0, loadCount * 16).length ? (
            <button
              onClick={() => setLoadCount(loadCount + 1)}
              className="load-more-btn"
            >
              ...LOAD MORE...
            </button>
          ) : null}
        </div>
        <div className="second-col">
          {!Object.keys(currentEmployee).length ? (
            <div className="project-intro">
              <h1>WELCOME TO MY DUMMY PROJECT</h1>
              <p>Hi tretton37.</p>
              <p>I'm Ali Jafarian, a Front-End developer from Iran.</p>
              <p> I made this dummy project with ReactJS and pure SCSS.</p>
              <p>It took me 6 hours to make. I hope you like it :)</p>
              <a
                className="gitlab-repo-btn"
                target="blank"
                href="https://gitlab.com/ajblog/tretton37public"
              >
                CLICK HERE TO CHECK IT OUT ON GITLAB
              </a>
            </div>
          ) : (
            <div className="current-employee">
              <h2>{currentEmployee.name}</h2>
              <div className="current-employee-maintext"></div>
            </div>
          )}
        </div>
      </div>
    );
  return <Loading />;
}
